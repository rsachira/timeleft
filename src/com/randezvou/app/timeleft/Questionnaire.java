package com.randezvou.app.timeleft;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;

public class Questionnaire extends SherlockActivity {
	
	public static final int CALCULATE = 224;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_questionnaire);
	}
	
	@Override
	public boolean onCreateOptionsMenu(com.actionbarsherlock.view.Menu menu) {
		
		menu.add(0, CALCULATE, 0, "Save").setIcon(R.drawable.abs__ic_cab_done_holo_dark).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
		int itemID = item.getItemId();
		switch(itemID){
		
		case CALCULATE:
			System.out.println("CALCULATE clicked");
			
			UserData uData = getData();
			if(uData == null)
				return true;
			
			Intent resultIntent = new Intent();
			resultIntent.putExtra(UserData.NAME, uData.getName());
			resultIntent.putExtra(UserData.GENDER, uData.getGender());
			resultIntent.putExtra(UserData.SMOKE, uData.getSmoke());
			resultIntent.putExtra(UserData.DRINK, uData.getDrink());
			resultIntent.putExtra(UserData.COUNTRY, uData.getCountry_value());
			resultIntent.putExtra(UserData.HEIGHT, uData.getHeight_value());
			resultIntent.putExtra(UserData.WEIGHT, uData.getWeight_value());
			resultIntent.putExtra(UserData.BMI, uData.calculateBMI());
			
			int[] bday = uData.getBirthday();
			resultIntent.putExtra(UserData.BIRTHDAY_DAY, Integer.toString(bday[0]) );
			resultIntent.putExtra(UserData.BIRTHDAY_MONTH, Integer.toString(bday[1]) );
			resultIntent.putExtra(UserData.BIRTHDAY_YEAR, Integer.toString(bday[2]) );
			
			setResult(Activity.RESULT_OK, resultIntent);
			finish();
			break;
		}
		
		return true;
	}
	
	private UserData getData(){
		
		String[] country_value_array = getResources().getStringArray(R.array.country_value_array);
		String[] feet_value_array = getResources().getStringArray(R.array.feet_value_array);
		String[] weight_value_array = getResources().getStringArray(R.array.weight_value_array);
		
		String[] country_array = getResources().getStringArray(R.array.country_array);
		String[] feet_array = getResources().getStringArray(R.array.feet_string_array);
		String[] weight_array = getResources().getStringArray(R.array.weight_string_array);
		
		EditText name = (EditText)findViewById(R.id.user_name);
		Spinner country = (Spinner)findViewById(R.id.country);
		Spinner height = (Spinner)findViewById(R.id.height);
		Spinner weight = (Spinner)findViewById(R.id.weight);
		
		DatePicker bday = (DatePicker)findViewById(R.id.birthday);
		
		RadioButton male = (RadioButton)findViewById(R.id.male);
		RadioButton smoke_yes = (RadioButton)findViewById(R.id.smoke_yes);
		RadioButton drink_yes = (RadioButton)findViewById(R.id.drink_yes);
		
		String user_name = name.getText().toString();
		if( user_name.equals("") ){
			
			showMessage("Please fill the name");
			return null;
		}
		
		//Values
		String country_value = country_value_array[ country.getSelectedItemPosition() ];
		String weight_value = weight_value_array[ weight.getSelectedItemPosition() ];
		String height_value = feet_value_array[ height.getSelectedItemPosition() ];
		
		//Strings
		String country_name = country_array[ country.getSelectedItemPosition() ];
		String weight_name = weight_array[ weight.getSelectedItemPosition() ];
		String height_name = feet_array[ height.getSelectedItemPosition() ];
		
		int[] birthday = new int[3];
		birthday[0] = bday.getDayOfMonth();
		birthday[1] = bday.getMonth() + 1;
		birthday[2] = bday.getYear();
		
		boolean isMale;
		if( male.isChecked() )
			isMale = true;
		else
			isMale = false;
		
		boolean smoke;
		if( smoke_yes.isChecked() )
			smoke = true;
		else
			smoke = false;
		
		boolean drink;
		if( drink_yes.isChecked() )
			drink = true;
		else
			drink = false;
		
		UserData userdata = new UserData(user_name, isMale, smoke, drink, country_value, country_name, height_value, height_name, weight_value, weight_name, birthday);
		return userdata;
	}
	
	private void showMessage(String message){
		
		AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);

		dlgAlert.setMessage(message);
		dlgAlert.setTitle("App Title");
		dlgAlert.setPositiveButton("OK", null);
		dlgAlert.setCancelable(true);
		dlgAlert.setPositiveButton("OK", new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				// TODO Auto-generated method stub
				arg0.dismiss();
			}
		});
		dlgAlert.create().show();
	}

}
