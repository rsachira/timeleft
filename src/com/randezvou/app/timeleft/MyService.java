package com.randezvou.app.timeleft;

import java.util.Date;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Typeface;
import android.os.IBinder;
import android.widget.RemoteViews;

public class MyService extends Service{

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		SharedPreferences prefs = getSharedPreferences("settings",0);
	 	String deathTime = prefs.getString("death_time", "");
	 	Date deathDate = MainActivity.stringToDate(deathTime, "d MMMM yyyy");
		Long milliSec_timeDiff = deathDate.getTime() - System.currentTimeMillis();
		
		String time = Long.toString(milliSec_timeDiff / 1000);
		RemoteViews views = new RemoteViews(getPackageName(), R.layout.widget_layout);
		views.setImageViewBitmap(R.id.update, buildUpdate(time) );
		
		
		ComponentName thisWidget = new ComponentName(this, CountDownAppWidgetProvider.class);
		AppWidgetManager manager = AppWidgetManager.getInstance(this);  
		manager.updateAppWidget(thisWidget, views); 
		
		return super.onStartCommand(intent, flags, startId);
	}
	
	public Bitmap buildUpdate(String time){
    	
        Bitmap myBitmap = Bitmap.createBitmap(560, 80, Bitmap.Config.ARGB_4444);
        Canvas myCanvas = new Canvas(myBitmap);
        Paint paint = new Paint();
        Typeface clock = Typeface.createFromAsset(getAssets(),"fonts/horror_font.ttf");
        paint.setAntiAlias(true);
        paint.setSubpixelText(true);
        paint.setTypeface(clock);
        paint.setStyle(Paint.Style.FILL);
        paint.setColor( getResources().getColor(R.color.orange) );
        paint.setTextSize(60);
        paint.setTextAlign(Align.CENTER);
        paint.setShadowLayer(1, 0, 0, getResources().getColor(R.color.black) );
        myCanvas.drawText(time, 280, 60, paint);
        return myBitmap;
    }
	
	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

}
