package com.randezvou.app.timeleft;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserData {
	
	public static final String NAME = "nom";
	public static final String GENDER = "sex";
	public static final String SMOKE = "smoke";
	public static final String DRINK = "drink";
	public static final String BIRTHDAY_DAY = "day";
	public static final String BIRTHDAY_MONTH = "month";
	public static final String BIRTHDAY_YEAR = "year";
	public static final String COUNTRY = "country";
	public static final String HEIGHT = "ftall";
	public static final String WEIGHT = "kg";
	public static final String BMI = "bmi";
	
	public static final int INCHES_PER_FOOT = 12;
	public static final double METERS_PER_INCH = 0.0254;
	
	private String name;
	private String gender;
	private boolean smoke;
	private boolean drink;
	private int[] birthday;
	private String country_name;
	private String country_value;
	private String height;
	private String height_value;
	private String weight;
	private String weight_value;
	
	public UserData(String username, boolean isMale, boolean smoker, boolean drinker, String country_val, String countryString, String height_val, String heightString, String weight_val, String weightString, int[] bday){
		// bday[0] = day, bday[1] = month, bday[2] = year
		name = username;
		if(isMale)
			gender = "m";
		else
			gender = "f";
		smoke = smoker;
		drink = drinker;
		
		country_value = country_val;
		country_name = countryString;
		
		height_value = height_val;
		height = heightString;
		
		weight_value = weight_val;
		weight = weightString;
		
		birthday = bday;
		
		calculateBMI();
	}
	
	private double heightInInches(){
		
		Pattern p = Pattern.compile("([0-9]{1})( ft )([0-9]{1,2})");
		Matcher m = p.matcher(height);
		m.find();
		
		String feetString = m.group(1);
		String inchString = m.group(3);
		
		int feets = Integer.parseInt(feetString);
		double inches = Double.parseDouble(inchString);
		
		inches += INCHES_PER_FOOT * feets;
		return inches;
	}
	
	private double heightInMeters(double heightInInches){
		
		return heightInInches * METERS_PER_INCH;
	}
	
	private int weightInKg(){
		
		Pattern p = Pattern.compile("([0-9]{1,3})( kg)");
		Matcher m = p.matcher(weight);
		
		if ( m.find() ){
			
			String kgString = m.group(1);
			return Integer.parseInt(kgString);
		}
		
		return 0;
	}
	
	public double calculateBMI(){
		
		double hInches = heightInInches();
		double hMeters = heightInMeters(hInches);
		int weightKg = weightInKg();
		
		double bmi = weightKg / Math.pow(hMeters, 2);
		return bmi;
	}

	public String getName() {
		return name;
	}

	public String getGender() {
		return gender;
	}

	public String getSmoke() {
		
		if(smoke)
			return "yes";
		
		return "no";
	}

	public String getDrink() {
		
		if(drink)
			return "yes";
		
		return "no";
	}

	public int[] getBirthday() {
		return birthday;
	}

	public String getCountry_name() {
		return country_name;
	}

	public String getCountry_value() {
		return country_value;
	}

	public String getHeight() {
		return height;
	}

	public String getHeight_value() {
		return height_value;
	}

	public String getWeight() {
		return weight;
	}
	
	public String getWeight_value() {
		return weight_value;
	}
	
	
}
