package com.randezvou.app.timeleft;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.Button;
import android.widget.TextView;

public class DeathTime extends AsyncTask<Void, Void, Void> {
	
	String htmlText;
	TextView tView;
	TextView bmiView;
	TextView deathTimer;
	TextView condition;
	
	Activity activity;
	Button calculateBtn;
	
	public DeathTime(TextView t, TextView deathTim, TextView bmi, TextView bmiCondition, Activity parent, Button btn){
		
		tView = t;
		bmiView = bmi;
		deathTimer = deathTim;
		condition = bmiCondition;
		htmlText = "nothing";
		activity = parent;
		calculateBtn = btn;
	}
	
	@Override
	protected Void doInBackground(Void... arg0) {
		
		List<NameValuePair> postParams = getPostParams();
		InputStream is = WebService.getData("http://www.deathtimer.com/index.php", postParams);
		htmlText = WebService.getText(is);
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
		
		Pattern p = Pattern.compile("[0-9]{1,2} [A-Z|a-z]* [0-9]{4}");
		Matcher m = p.matcher(htmlText);
		if( m.find() ){
			
			Preferences.deathTime = m.group();
			Preferences.firstRunComplete();
			Preferences.savePreferences();
			activity.setProgressBarIndeterminateVisibility(false);
			calculateBtn.setEnabled(true);
			MainActivity.startTimer(activity, deathTimer, tView, bmiView, condition);
			return;
		}
		
		if( htmlText.equals("") ){
			
			tView.setText("Connection Lost.");
			calculateBtn.setEnabled(true);
			return;
		}
		tView.setText("You are Living on Borrowed Time.");
		bmiView.setText( Float.toString(Preferences.BMI) );
		activity.setProgressBarIndeterminateVisibility(false);
		calculateBtn.setEnabled(true);
	}
	
	private List<NameValuePair> getPostParams(){
		
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(11);
		nameValuePairs.add(new BasicNameValuePair("nom", Preferences.name ));
		nameValuePairs.add(new BasicNameValuePair("sex", Preferences.gender ));
		nameValuePairs.add(new BasicNameValuePair("smoke", Preferences.smoke ));
		nameValuePairs.add(new BasicNameValuePair("drink", Preferences.drink ));
		nameValuePairs.add(new BasicNameValuePair("go", " Go! "));
		nameValuePairs.add(new BasicNameValuePair("day", Preferences.day ));
		nameValuePairs.add(new BasicNameValuePair("month", Preferences.month ));
		nameValuePairs.add(new BasicNameValuePair("year", Preferences.year ));
		nameValuePairs.add(new BasicNameValuePair("country", Preferences.country_value ));
		nameValuePairs.add(new BasicNameValuePair("ftall", Preferences.height_value ));
		nameValuePairs.add(new BasicNameValuePair("kg", Preferences.weight_value ));
		
		return nameValuePairs;
	}

}
