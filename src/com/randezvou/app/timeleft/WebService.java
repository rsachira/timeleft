package com.randezvou.app.timeleft;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class WebService {

	public static String getJokeUrl(String title){
		
		String t = title.replace(" ", "%20");
		String url = "http://jokewiki-anarcxh.rhcloud.com/mediawiki/api.php?action=parse&page=" + t + "&format=xml";
		
		return url;
	}
	
	public static String getRandomPosts(int num){
		
		String url = "http://jokewiki-anarcxh.rhcloud.com/mediawiki/api.php?action=query&list=random&rnlimit=" + Integer.toString(num) + "&format=xml";
		
		return url;
	}
	
	public static InputStream getData(String url){
		
		InputStream is = null;
		
		try{
	        	
	        	HttpClient httpclient = new DefaultHttpClient();
	        	HttpPost httppost = new HttpPost(url);
	        	HttpResponse response = httpclient.execute(httppost);
	        	HttpEntity entity = response.getEntity();
	        
	        	is = entity.getContent();
	        	
	         
	        
	        }catch(Exception e){
	        
	        	Log.e("log_tag", "Error in http connection "+e.toString());
	        
	        }
		
		return is;
	}
	
	public static InputStream getData(String url, List<NameValuePair> postParams){
		
		InputStream is = null;
		
		try{
	        	
	        	HttpClient httpclient = new DefaultHttpClient();
	        	HttpPost httppost = new HttpPost(url);
	        	httppost.setEntity(new UrlEncodedFormEntity(postParams) );
	        	HttpResponse response = httpclient.execute(httppost);
	        	HttpEntity entity = response.getEntity();
	        	
	        	is = entity.getContent();
	        
	        }catch(Exception e){
	        
	        	Log.e("log_tag", "Error in http connection "+e.toString());
	        
	        }
		
		return is;
	}
	
	public static String getText(InputStream is){
		
		String result = "";
		
		try{
	        
	        	BufferedInputStream reader = new BufferedInputStream(is);
	        	StringBuilder sb = new StringBuilder();
	        	byte[] buffer = new byte[65536]; 
	        
	        	int len = 0;
	        	
	        	while ((len = reader.read(buffer)) != -1) {
	        		
	        		//sb.append(line + "\n");
	        		sb.append(new String(buffer, 0, len, "UTF-8"));
	        	}
	        
	        	is.close();
	        
	        	result=sb.toString();
	        
	        }catch(Exception e){
	        
	        	Log.e("log_tag", "Error converting result "+e.toString());
	        
	        }
		
		return result;
	}
	
	public static boolean isNetworkConnected(Activity activity) {
		
		  
		ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		  
		if (ni == null) {
		   // There are no active networks
			return false;
		  
		}
		
		return true;
	}
	
}
