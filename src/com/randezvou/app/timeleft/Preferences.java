package com.randezvou.app.timeleft;

import android.app.Activity;
import android.content.SharedPreferences;

public class Preferences {
	
	public static SharedPreferences settings;
	
	public static boolean firstRun = true;
	public static Float BMI = 0.0f;
	public static String name = "";
	public static String country_value = "";
	public static String height_value = "";
	public static String weight_value = "";
	public static String day = "";
	public static String month = "";
	public static String year = "";
	public static String gender = "";
	public static String smoke = "";
	public static String drink = "";
	
	public static String deathTime = "";
	
	public static void initialize(Activity activity){
		
		settings = activity.getSharedPreferences("settings", 0);
		
		firstRun = settings.getBoolean("first_run", firstRun);
		name = settings.getString("name", name);
		country_value = settings.getString("country_value", country_value);
		height_value = settings.getString("height_value", height_value);
		weight_value = settings.getString("weight_value", weight_value);
		BMI = settings.getFloat("bmi", BMI);
		
		day = settings.getString("birthday_day", day);
		month = settings.getString("birthday_month", month);
		year = settings.getString("birthday_year", year);
		
		gender = settings.getString("gender", gender);
		smoke = settings.getString("smoke", smoke);
		drink = settings.getString("drink", drink);
		
		deathTime = settings.getString("death_time", deathTime);
	}
	
	public static void savePreferences(){
		
		SharedPreferences.Editor editor = settings.edit();
		
		editor.putString("name", name);
		editor.putString("country_value", country_value);
		editor.putString("height_value", height_value);
		editor.putString("weight_value", weight_value);
		editor.putFloat("bmi", BMI);
		
		editor.putString("birthday_day", day);
		editor.putString("birthday_month", month);
		editor.putString("birthday_year", year);
		
		editor.putString("gender", gender);
		editor.putString("smoke", smoke);
		editor.putString("drink", drink);
		
		editor.putString("death_time", deathTime);
		
		editor.putBoolean("first_run", firstRun);
		
		editor.commit();
	}
	
	public static void firstRunComplete(){
		
		firstRun = false;
	}
	
	public static void isFirstRun(){
		
		firstRun = true;
	}
	
	public static void saveDeathTime(){
		
		
	}
	
	public static String makeTwoDigit(String num){
		
		if(num.length() > 1)
			return num;
		
		num = '0' + num;
		return num;
	}
	
}
