package com.randezvou.app.timeleft;

import java.util.Date;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.widget.TextView;

public class TimeToDie extends TimerTask {
	
	private Activity activity;
	private TextView timer;
	private Date deadDate;
	Long diffInSec;
	
	public TimeToDie(Date demiseDate, Activity a, TextView timerView){
		
		activity = a;
		timer = timerView;
		deadDate = demiseDate;
	}
	
	@Override
	public void run() {
		
		Long milliSec_timeDiff = deadDate.getTime() - System.currentTimeMillis();
		diffInSec = TimeUnit.MILLISECONDS.toSeconds(milliSec_timeDiff);
		
		activity.runOnUiThread(new Runnable() {
			
			@Override
			public void run() {
				timer.setText( Long.toString(diffInSec) );
				if(timer.getLineCount() > 1)
					timer.setTextSize( timer.getTextSize() - 1 );
			}
		});
		
	}
	
}
