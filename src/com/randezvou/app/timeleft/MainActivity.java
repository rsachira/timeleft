package com.randezvou.app.timeleft;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Timer;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	private static final int QUESTIONNAIRE_ACTIVITY = 5324;
	
	Button calButton;
	TextView deathDate;
	TextView bmiView;
	TextView deathTimer;
	
	TextView bmiDesc;
	TextView conditionDesc;
	TextView condition;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setContentView(R.layout.activity_main);
		
		Typeface horror_font = Typeface.createFromAsset(getAssets(), "fonts/horror_font.ttf");
		
		deathDate = (TextView)findViewById(R.id.death_date);
		calButton = (Button)findViewById(R.id.calculate);
		bmiView = (TextView)findViewById(R.id.bmi);
		bmiDesc = (TextView)findViewById(R.id.bmi_desc);
		deathTimer = (TextView)findViewById(R.id.death_timer);
		condition = (TextView)findViewById(R.id.condition);
		conditionDesc = (TextView)findViewById(R.id.condition_desc);
		
		//Set Fonts
		deathDate.setTypeface(horror_font);
		calButton.setTypeface(horror_font);
		deathTimer.setTypeface(horror_font);
		bmiView.setTypeface(horror_font);
		bmiDesc.setTypeface(horror_font);
		condition.setTypeface(horror_font);
		conditionDesc.setTypeface(horror_font);
		
		//Set Font Size
		deathTimer.setTextSize( getResources().getDimension(R.dimen.timer_text_size) );
		
		Preferences.initialize(this);
		if( !Preferences.firstRun )
			startTimer(this, deathTimer, deathDate, bmiView, condition);
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		
		if(resultCode != Activity.RESULT_OK)
    		return;
		
		switch(requestCode){
		
		case QUESTIONNAIRE_ACTIVITY:
			this.setProgressBarIndeterminateVisibility(true);
			calButton.setEnabled(false);
			getParameters(data);
			
			new DeathTime(deathDate, deathTimer, bmiView, condition, this, calButton).execute();
			break;
		}
		
	}
	
	private void getParameters(Intent data){
		
		Preferences.name = data.getStringExtra(UserData.NAME);
		Preferences.gender = data.getStringExtra(UserData.GENDER);
		Preferences.smoke = data.getStringExtra(UserData.SMOKE);
		Preferences.drink = data.getStringExtra(UserData.DRINK);
		Preferences.day = Preferences.makeTwoDigit( data.getStringExtra(UserData.BIRTHDAY_DAY) );
		Preferences.month = data.getStringExtra(UserData.BIRTHDAY_MONTH);
		Preferences.year = data.getStringExtra(UserData.BIRTHDAY_YEAR);
		Preferences.country_value = data.getStringExtra(UserData.COUNTRY);
		Preferences.height_value = data.getStringExtra(UserData.HEIGHT);
		Preferences.weight_value = data.getStringExtra(UserData.WEIGHT);
		Preferences.BMI = (float)data.getDoubleExtra(UserData.BMI, 0);
		
		Preferences.isFirstRun();
		Preferences.savePreferences();
	}
	
	public void calculateClicked(View view){
		
		Intent favIntent = new Intent(this, Questionnaire.class);
        startActivityForResult(favIntent, QUESTIONNAIRE_ACTIVITY);
	}
	
	public static Date stringToDate(String aDate,String aFormat) {

	      if(aDate==null)
	    	  return null;
	      
	      ParsePosition pos = new ParsePosition(0);
	      SimpleDateFormat simpledateformat = new SimpleDateFormat(aFormat);
	      Date stringDate = simpledateformat.parse(aDate, pos);
	      
	      return stringDate;
	}
	
	public static void startTimer(Activity a, TextView deathTimerView, TextView deathDate, TextView bmiView, TextView conditionView){
		
		Date dDate = stringToDate(Preferences.deathTime, "d MMMM yyyy");
		
		deathDate.setText( Preferences.deathTime );
		bmiView.setText( Float.toString(Preferences.BMI) );
		
		conditionView.setText( condition(Preferences.BMI) );
		
		Timer deathTimer = new Timer();
		deathTimer.schedule(new TimeToDie(dDate, a, deathTimerView), 0, 100);
	}
	
	private static String condition(float bmi){
		
		if(bmi <= 18)
			return "Underweight";
		
		if(bmi <= 18.5f)
			return "Thin for Height";
		
		if(bmi <= 24.9f)
			return "Normal";
		
		if(bmi <= 29.9)
			return "Overweight";
		
		return "Obese";
	}

}
